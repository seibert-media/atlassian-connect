// eslint-disable-next-line @typescript-eslint/no-var-requires
const {nodeExternalsPlugin} = require("esbuild-node-externals");

// eslint-disable-next-line @typescript-eslint/no-var-requires
require("esbuild")
	.build({
		entryPoints: ["src/index.ts"],
		bundle: true,
		minify: true,
		outfile: "out/index.js",
		platform: "node",
		target: "node12",
		sourcemap: false,
		plugins: [nodeExternalsPlugin()],
	})
	.then(r => console.log("esbuild finished ⚡️", r))
	.catch(e => console.error("esbuild failed ☹️", e));
