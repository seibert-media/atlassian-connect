import {Response as ExpressResponse} from "express-serve-static-core";
import sanitizeHtml from "sanitize-html";

export function sendError({status = 401, message, res}: { status?: number, message: string, res: ExpressResponse }): void {
	const sanitizedMessage = sanitizeHtml(message, {
		allowedTags: [],
		allowedAttributes: {},
	});
	res.status(status).send(sanitizedMessage);
}
