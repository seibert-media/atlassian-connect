import {
	AuthenticatedAtlassianRequest,
	composeAtlassianRequestAuthenticationMiddleware
} from "./authenticate-middleware";
import {Response as ExpressResponse} from "express-serve-static-core";
import {createJwtForRequestAuthorization} from "./jwt/create-jwt";

const getMockRequest = ({
	authorizationHeader,
	body,
	path,
	method
}: { authorizationHeader?: string, body?: unknown, path?: string, method?: string }) => {
	const getMock = jest.fn(arg => {
		if (arg === "Authorization") {
			return authorizationHeader;
		}
	});
	return {
		get: getMock,
		body,
		path,
		method
	};
};

const getMockResponse = () => {
	return {
		sendStatus: jest.fn(),
		status: jest.fn().mockReturnThis(),
		send: jest.fn().mockReturnThis(),
	};
};

test("Rejects on missing jwt", () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({
			clientKey: "tenant-key",
			sharedSecret: "tenants-shared-secret"
		}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Could not find JWT."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects on broken jwt", () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({
			clientKey: "tenant-key",
			sharedSecret: "tenants-shared-secret"
		}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "foo"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Invalid JWT."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects on missing iss claim", () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({
			clientKey: "tenant-key",
			sharedSecret: "tenants-shared-secret"
		}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("JWT did not contain the issuer (iss) claim"));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if fetching client information fails", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.reject(),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28ifQ.iCXw6MRK2cNTL5w3IYx6wj8-qxuokIUG-Bg-dKb1Mm0"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Request could not be validated."));
	expect(mockResponse.status).toBeCalledWith(400);
});

test("Rejects if fetched client is missing sharedSecret", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: undefined}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28ifQ.iCXw6MRK2cNTL5w3IYx6wj8-qxuokIUG-Bg-dKb1Mm0"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Could not obtain shared secret stored for client"));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if client can not get fetched", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve(null),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28ifQ.iCXw6MRK2cNTL5w3IYx6wj8-qxuokIUG-Bg-dKb1Mm0"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Could not obtain shared secret stored for client"));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt is signed with wrong secret", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28ifQ.okUBSMvzLc8bBgz92eytxeNOJGf4uFzeSguRUyUDYs0"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Unable to decode JWT."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt is missing exp", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28ifQ.IluMo_5ZfTgwEZooEy-WJsmDb1GA3Z4xOQ34pLTr800"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Jwt token invalid. Does not contain an valid exp claim."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt provided invalid exp", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOiJmb28ifQ.Vl4HrdWzQjIkyOM_oD9OklSQ8pcpjFkTyqQtfa95qeY"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Jwt token invalid. Does not contain an valid exp claim."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt provided invalid alg preventing xss injection in error response", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJmYWtlPHNjcmlwdCBzcmM9aHR0cHM6Ly9oYWNrLmNvbT48L3NjcmlwdD4ifQ==.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOiJmb28ifQ.Vl4HrdWzQjIkyOM_oD9OklSQ8pcpjFkTyqQtfa95qeY"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Invalid JWT.Error: Algorithm from the header \"fake\""));
	expect(mockResponse.send).toBeCalledWith(expect.not.stringContaining("script"));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt provided expired exp", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOjF9.VNYX3XBTebTN7s_iR9MTUe3UVOdFxN1Mp4Cu38nZKRo"}});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Jwt token invalid. Does not contain an valid exp claim."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt provided invalid qsh for express request", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({
		path: "/",
		method: "GET",
		body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOjEwMDAwMDAwMDAwMDAwMDAwMDB9.TaoUIL4Lhb5lnO0uqUmYfFz9FT-PeFT6L_GnlKTUzAk"}
	});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Jwt token invalid. Does not contain an valid qsh claim."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects if jwt provided invalid qsh for given custom requests", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
		requestsForQshValidation: [{pathname: "/admin-only", method: "GET"}, {
			pathname: "/super-user-only",
			method: "GET"
		}]
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({
		path: "/",
		method: "GET",
		body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOjEwMDAwMDAwMDAwMDAwMDAwMDB9.TaoUIL4Lhb5lnO0uqUmYfFz9FT-PeFT6L_GnlKTUzAk"}
	});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Jwt token invalid. Does not contain an valid qsh claim."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Passes request if jwt is valid (second custom request matches qsh)", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
		requestsForQshValidation: [{pathname: "/admin-only", method: "GET"}, {
			pathname: "/super-user-only",
			method: "GET"
		}, {pathname: "/foo", method: "PATH"}]
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({
		path: "/",
		method: "GET",
		body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOjEwMDAwMDAwMDAwMDAwMDAwMDAsInFzaCI6Ijg5NmYxNGY2ZDAwNzk2N2UxMWQxYzc0ZWUxNzkzZWRhNzU0NThiMTJiODA0NzQyY2ZiMDIxMjY2N2Y3MWQwMzMifQ.yDBrXN4q_nzN2kabQ7xFyD2dNgpFOMt-QimYQyXz8EA"}
	});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockNext).toBeCalled();
});

test("Passes request if jwt is valid (express Request matches qsh)", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({
		path: "/",
		method: "GET",
		body: {jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJmb28iLCJleHAiOjEwMDAwMDAwMDAwMDAwMDAwMDAsInFzaCI6ImM4OGNhYWQxNWExYzFhOTAwYjhhYzA4YWE5Njg2ZjRlODE4NDUzOWJlYTFkZWRhMzZlMmY2NDk0MzBkZjMyMzkifQ.rhTaCRHY3QcRHW9cDkyYCKQM9yHwHgjNi1bxnMVQITw"}
	});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockNext).toBeCalled();
});

test("Passes request if jwt is valid (request from create-jwt method matches)", async () => {
	const middleware = composeAtlassianRequestAuthenticationMiddleware({
		baseUrl: "https://your.apps.base.url",
		fetchTenantByClientKey: () => Promise.resolve({sharedSecret: "secret"}),
	});
	const mockNext = jest.fn();
	const mockRequest = getMockRequest({
		path: "/admin/only",
		method: "GET",
		body: {
			jwt: createJwtForRequestAuthorization({
				baseUrl: "https://your.apps.base.url",
				expirationInSeconds: 30000,
				issuer: "foo",
				request: {
					pathname: "/admin/only",
					method: "GET"
				},
				tenantData: {sharedSecret: "secret", clientKey: "foo"}
			})
		}
	});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedAtlassianRequest, mockResponse as unknown as ExpressResponse, mockNext);
	await new Promise(setImmediate);
	expect(mockNext).toBeCalled();
});

