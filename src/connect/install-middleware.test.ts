import {Response} from "express-serve-static-core";
import {AuthenticatedInstallationRequest, composeAtlassianConnectInstallationMiddleware} from "./install-middleware";
import {AsymmetricAlgorithm, encodeAsymmetric} from "atlassian-jwt";
import {fetchRsaPublicKey} from "./jwt/fetch-rsa-key";
import {PRIVATE_KEY_FOR_TESTING_PURPOSES, PUBLIC_KEY_FOR_TESTING_PURPOSES} from "../keys-for-tests";

jest.mock("./jwt/fetch-rsa-key", () => {
	return {
		__esModule: true,
		fetchRsaPublicKey: jest.fn()
	};
});

const testBaseUrl = "https://example.com";
const middleware = composeAtlassianConnectInstallationMiddleware({baseUrl: testBaseUrl});

const getMockRequest = ({
	authorizationHeader,
	body,
	path,
	method
}: { authorizationHeader?: string, body?: unknown, path?: string, method?: string }) => {
	const getMock = jest.fn(arg => {
		if (arg === "Authorization") {
			return authorizationHeader;
		}
	});
	return {
		get: getMock,
		body,
		path,
		method
	};
};

const getMockResponse = () => {
	return {
		sendStatus: jest.fn(),
		status: jest.fn().mockReturnThis(),
		send: jest.fn().mockReturnThis(),
	};
};

test("Rejects empty Authorization header", () => {
	const mockRequest = getMockRequest({authorizationHeader: undefined});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	expect(mockResponse.status).toBeCalledWith(401);
	expect(mockResponse.send).toBeCalledWith("Could not find Authorization header.");
});

test("Rejects broken Authorization header", () => {
	const mockRequest = getMockRequest({authorizationHeader: "foo"});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	expect(mockResponse.status).toBeCalledWith(400);
	expect(mockResponse.send).toBeCalledWith("Jwt header has an unexpected form.");
});

test("Rejects broken jwt", () => {
	const mockRequest = getMockRequest({authorizationHeader: "JWT foo"});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	expect(mockResponse.status).toBeCalledWith(400);
	expect(mockResponse.send).toBeCalledWith("Jwt header has an unexpected form.");
});

test("Rejects jwt missing kid in header", () => {
	const jwt = encodeAsymmetric({}, PRIVATE_KEY_FOR_TESTING_PURPOSES, AsymmetricAlgorithm.RS256, {kid: undefined});
	const mockRequest = getMockRequest({authorizationHeader: "JWT " + jwt});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	expect(mockResponse.status).toBeCalledWith(400);
	expect(mockResponse.send).toBeCalledWith("Jwt header has an unexpected form.");
});

test("Rejects broken installation payload", () => {
	const jwt = encodeAsymmetric({}, PRIVATE_KEY_FOR_TESTING_PURPOSES, AsymmetricAlgorithm.RS256, {kid: "123"});
	const mockRequest = getMockRequest({authorizationHeader: "JWT " + jwt});
	const mockResponse = getMockResponse();
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	expect(mockResponse.status).toBeCalledWith(401);
	expect(mockResponse.send).toBeCalledWith("Could not find clientKey on body.");
});

test("Rejects malicious jwt ", async () => {
	const jwt = encodeAsymmetric({
		aud: [testBaseUrl],
		iss: "xxx-yyy",
		//qsh: "c88caad15a1c1a900b8ac08aa9686f4e8184539bea1deda36e2f649430df3239",
		qsh: "fake",
		exp: ((new Date()).getTime() / 1000) * 20000
	}, PRIVATE_KEY_FOR_TESTING_PURPOSES, AsymmetricAlgorithm.RS256, {kid: "123"});
	const mockRequest = getMockRequest({
		authorizationHeader: "JWT " + jwt,
		body: {clientKey: "xxx-yyy"},
		method: "GET"
	});
	const mockResponse = getMockResponse();
	const middleware = composeAtlassianConnectInstallationMiddleware({baseUrl: testBaseUrl});
	(fetchRsaPublicKey as jest.Mock).mockReturnValue(Promise.resolve(PUBLIC_KEY_FOR_TESTING_PURPOSES));
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	const flushPromises = () => new Promise(setImmediate);
	await flushPromises();
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Could not decode jwt token.Error: Jwt token invalid."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Rejects on error on public key retrieval", async () => {
	const jwt = encodeAsymmetric({
		aud: [testBaseUrl],
		iss: "xxx-yyy",
		//qsh: "c88caad15a1c1a900b8ac08aa9686f4e8184539bea1deda36e2f649430df3239",
		qsh: "fake",
		exp: ((new Date()).getTime() / 1000) * 20000
	}, PRIVATE_KEY_FOR_TESTING_PURPOSES, AsymmetricAlgorithm.RS256, {kid: "123"});
	const mockRequest = getMockRequest({
		authorizationHeader: "JWT " + jwt,
		body: {clientKey: "xxx-yyy"},
		method: "GET"
	});
	const mockResponse = getMockResponse();
	const middleware = composeAtlassianConnectInstallationMiddleware({baseUrl: testBaseUrl});
	(fetchRsaPublicKey as jest.Mock).mockImplementation(() => Promise.reject("Failed."));
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, null);
	const flushPromises = () => new Promise(setImmediate);
	await flushPromises();
	expect(mockResponse.send).toBeCalledWith(expect.stringContaining("Could not obtain rsaPublicKeyFailed."));
	expect(mockResponse.status).toBeCalledWith(401);
});

test("Passes valid jwtBody ", async () => {
	const jwtBody = {
		aud: [testBaseUrl],
		iss: "xxx-yyy",
		qsh: "c88caad15a1c1a900b8ac08aa9686f4e8184539bea1deda36e2f649430df3239",
		exp: ((new Date()).getTime() / 1000) * 20000
	};
	const jwt = encodeAsymmetric(jwtBody, PRIVATE_KEY_FOR_TESTING_PURPOSES, AsymmetricAlgorithm.RS256, {kid: "123"});
	const mockRequest = getMockRequest({
		authorizationHeader: "JWT " + jwt,
		body: {clientKey: "xxx-yyy"},
		method: "GET"
	});
	const mockResponse = getMockResponse();
	const middleware = composeAtlassianConnectInstallationMiddleware({baseUrl: testBaseUrl});
	(fetchRsaPublicKey as jest.Mock).mockReturnValue(Promise.resolve(PUBLIC_KEY_FOR_TESTING_PURPOSES));
	const nextMock = jest.fn();
	middleware(mockRequest as unknown as AuthenticatedInstallationRequest, mockResponse as unknown as Response, nextMock);
	const flushPromises = () => new Promise(setImmediate);
	await flushPromises();
	expect((mockRequest as unknown as { validatedJwtPayload: unknown }).validatedJwtPayload).toEqual(jwtBody);
	expect(nextMock).toBeCalled();
});
