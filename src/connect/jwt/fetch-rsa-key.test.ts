import {fetchRsaPublicKey} from "./fetch-rsa-key";
import fetch from "node-fetch";

jest.mock("node-fetch", () => {
	return {
		__esModule: true,
		default: jest.fn(),
	};
});

test("Returns null on non 2xx code", async () => {
	(fetch as jest.Mock).mockImplementation(() => {
		return Promise.resolve({ok: false});
	});
	await expect(() => fetchRsaPublicKey({url: "https://example.com"})).rejects.toThrow();
});

test("Returns key from remoteHost", async () => {
	(fetch as jest.Mock).mockImplementation(() => {
		return Promise.resolve({
			ok: true,
			text: () => Promise.resolve("b-l-a"),
		});
	});
	expect(await fetchRsaPublicKey({url: "https://example.com"})).toEqual("b-l-a");
});
