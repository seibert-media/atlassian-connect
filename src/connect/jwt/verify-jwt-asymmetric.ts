import {Request, decodeAsymmetric} from "atlassian-jwt";
import {AsymmetricAlgorithm} from "atlassian-jwt";
import {verifyQueryHash} from "./verify-query-hash";
import {JwtPayload} from "jsonwebtoken";

/**
 * verifies an encoded jwt against a given request and a RSA public key for asymmetric decoding.
 * it verifies the aud, iss, qsh and exp claim of the token as outlined in the Atlassian documentation:
 * @see https://developer.atlassian.com/cloud/jira/platform/understanding-jwt-for-connect-apps/#claims
 *
 * Asymmetric encryption is used in Atlassian Connect ecosystem for install and uninstall lifecycle hooks:
 * @see https://community.developer.atlassian.com/t/action-required-atlassian-connect-installation-lifecycle-security-improvements/49046
 *
 * returns the jwt body if validation succeeds
 *
 * @param encodedJwt
 * @param rsaPublicKey
 * @param expectedAudience
 * @param expectedIssuer
 * @param request
 */
export function verifyJwtAsymmetric(encodedJwt: string, rsaPublicKey: string, {
	expectedAudience,
	expectedIssuer,
	request
}: VerificationOptions): JwtPayload {
	const jwtBody: JwtPayload = decodeAsymmetric(encodedJwt, rsaPublicKey, AsymmetricAlgorithm.RS256, false);

	const normalizeUrl = (url: string) => url.replace(/\/$/, ""); // removes last slash if present
	const normalizedExpectedAudience = normalizeUrl(expectedAudience);

	if (!jwtBody.aud || !Array.isArray(jwtBody.aud)) {
		throw new Error(`Jwt token invalid. It is missing a valid aud claim. ${jwtBody.aud}`);
	}

	if (!jwtBody.aud.map(normalizeUrl).includes(normalizedExpectedAudience)) {
		throw new Error(`Jwt token invalid. Expected audience not part of jwt tokens audiences. (${jwtBody.aud})`);
	}

	if (!jwtBody.iss) {
		throw new Error("Jwt token invalid. It is missing an iss claim.");
	}

	if (jwtBody.iss !== expectedIssuer) {
		throw new Error("Jwt token invalid. Invalid issuer.");
	}

	if (!jwtBody.qsh) {
		throw new Error("Jwt token invalid. Does not contain qsh claim.");
	}

	const {successful, error} = verifyQueryHash({
		baseUrl: expectedAudience,
		qsh: jwtBody.qsh,
		request
	});

	if (!successful) {
		throw new Error(`Jwt token invalid. Query hash mismatch. ${error}`);
	}

	if (!jwtBody.exp || typeof jwtBody.exp !== "number") {
		throw new Error("Jwt token invalid. Does not contain exp claim.");
	}

	const nowUNIX = Math.floor((new Date()).getTime() / 1000);
	if (nowUNIX >= jwtBody.exp) {
		throw new Error("Jwt token invalid. Expired token.");
	}

	return jwtBody;
}

interface VerificationOptions {
	expectedAudience: string;
	expectedIssuer: string;
	request: Request;
}
