import {
	createQueryStringHash, encodeSymmetric,
	Request as AtlassianJwtRequest,
	SymmetricAlgorithm
} from "atlassian-jwt";
import {TenantDataWithClientKey, TenantDataWithSharedSecret} from "../middleware-common";
import {JwtPayload} from "jsonwebtoken";

/**
 * Creates a JWT that can be used for authentication of requests by providing it in the Authorization header as "JWT xxx".
 * @param tenantData
 * Data object you have stored for the Atlassian tenant. It has to contain a sharedSecret (used for JWT signing) and a
 * clientKey (used as the JWT audience).
 * @param issuer
 * Issuer string to be passed as the JWTs iss claim. Should be set to the app-key if you use this token for api calls
 * in server-to-server communication with atlassian host products.
 * @param expirationInSeconds
 * JWT expiration time in seconds. defaults to 300.
 * @param method
 * HTTP method of the signed request (GET, POST, ...). defaults to GET.
 * @param pathname
 * Pathname of the signed request (sth. like "/rest/api/content"), used to generate query hash (qsh claim).
 * @param body
 * Body of the signed request, used to generate query hash (qsh claim).
 * @param query
 * Query params of the signed request, used to generate query hash (qsh claim).
 * @param baseUrl
 * BaseUrl of the requests. Gets passed as baseUrl param to Atlassians jwt library. Can be omitted if pathname is set correctly
 * @param additionalClaims
 * Additional claims to be appended to JWT payload. Can be used to override aspects of the JWT or to enrich it with additional context information like a userId
 *
 * @example
 * usage for api calls to confluence
 * ```ts
 * const jwt = createJwtForRequestAuthorization({
 *		issuer: "my-app-key",
 *		request: {pathname: "rest/api/content", method: "GET"},
 *		tenantData: {
 *			clientKey: "tenant-key-of-installation-cb1f7a7ee297",
 *			sharedSecret: "sharedSecretOfClientFromInstallationProcess"
 *		}
 *	});
 * ```
 *
 * @example
 * usage for authentication against middleware function from authenticate-middleware.ts with fictitious path
 * ```ts
 * // 1. generate jwt
 * const jwt = createJwtForRequestAuthorization({
 *		issuer: "tenant-key-of-installation-cb1f7a7ee297",
 *		request: {pathname: "/admin-only", method: "GET"},
 *		tenantData: {
 *			clientKey: "tenant-key",
 *			sharedSecret: "tenants-shared-secret"
 *		}
 *	});
 *
 * // ... pass it to the client ...
 *
 * // 2. validate with middleware
 * const middleware = composeAtlassianRequestAuthenticationMiddleware({
 *		baseUrl: "https://your.apps.base.url",
 *		fetchTenantByClientKey: (clientKey: string) => Promise.resolve({
 *			clientKey: "tenant-key",
 *			sharedSecret: "tenants-shared-secret"
 *		}),
 *		requestsForQshValidation: [{method: "GET", pathname: "/admin-only"}]
 *	});
 *
 * // 3. use middleware for authentication
 * app.post("/some/endpoint/", [middleware], async(req: Request, res: Response) => {
 *     res.send();
 * });
 * ```
 **/
export function createJwtForRequestAuthorization({
	tenantData,
	issuer,
	expirationInSeconds = 300,
	request: {method = "GET", pathname = "", body, query},
	baseUrl,
	additionalClaims = {},
}: CreateJwtForRequestParams): string {

	const nowUNIXInSeconds = Math.floor((new Date()).getTime() / 1000);
	const payload = {
		iss: issuer,
		iat: nowUNIXInSeconds,
		exp: nowUNIXInSeconds + expirationInSeconds,
		aud: [tenantData.clientKey],
		qsh: createQueryStringHash({method, pathname, body, query}, !!body, baseUrl),
		...additionalClaims,
	};
	return encodeSymmetric(payload, tenantData.sharedSecret, SymmetricAlgorithm.HS256);
}

interface CreateJwtForRequestParams {
	tenantData: TenantDataWithSharedSecret & TenantDataWithClientKey,
	issuer: string,
	expirationInSeconds?: number,
	request: Partial<AtlassianJwtRequest>,
	baseUrl?: string,
	additionalClaims?: Partial<JwtPayload>
}
