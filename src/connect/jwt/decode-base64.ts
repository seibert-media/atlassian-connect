/**
 * initializes JSON object from stringified base64 representation. returns null on error.
 *
 * @param encoded
 */
export function decodeObjectFromBase64<T>(encoded: string): T | null {
	try {
		return JSON.parse(Buffer.from(encoded, "base64").toString());
	} catch (e) {
		console.error("Could not decode JWT header. Returning null instead.", e);
		return null;
	}
}
