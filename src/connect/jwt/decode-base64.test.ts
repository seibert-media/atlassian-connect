import {decodeObjectFromBase64} from "./decode-base64";

test("Returns null on invalid encoding", () => {
	const encodedHeader = "some-invalid-encoding";
	expect(decodeObjectFromBase64(encodedHeader)).toEqual(null);
});

test("Returns decoded header", () => {
	const header = {kid: 123};
	const encodedHeader = Buffer.from(JSON.stringify(header)).toString("base64");
	expect(decodeObjectFromBase64(encodedHeader)).toEqual(header);
});
