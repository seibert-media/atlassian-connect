import {extractJwt} from "./extract-jwt";
import {Request as ExpressRequest} from "express-serve-static-core";

test("Returns null if no JWT in query and no body is present", () => {
	const jwt = extractJwt({query: undefined, body: undefined, headers: {authorization: "JWT eysomejwt"}} as unknown as ExpressRequest);
	expect(jwt).toEqual(null);
});

test("Returns null if JWT present in query and in body", () => {
	const jwt = extractJwt({query: {jwt: "eysomejwt"}, body: {jwt: "eysomejwt"}, headers: undefined} as unknown as ExpressRequest);
	expect(jwt).toEqual(null);
});

test("Returns jwt from query if present", () => {
	const jwt = extractJwt({query: {jwt: "eysomejwt"}, body: undefined, headers: undefined} as unknown as ExpressRequest);
	expect(jwt).toEqual("eysomejwt");
});

test("Returns jwt from query if present", () => {
	const jwt = extractJwt({query: {jwt: "eysomejwt"}, body: undefined, headers: undefined} as unknown as ExpressRequest);
	expect(jwt).toEqual("eysomejwt");
});

test("Returns jwt from body if present", () => {
	const jwt = extractJwt({query: undefined, body: {jwt: "eysomejwt"}, headers: undefined} as unknown as ExpressRequest);
	expect(jwt).toEqual("eysomejwt");
});

test("Returns jwt from headers if present", () => {
	const jwt = extractJwt({query: undefined, body: {foo: 123}, headers: {authorization: "JWT eysomejwt"}} as unknown as ExpressRequest);
	expect(jwt).toEqual("eysomejwt");
});

test("Returns null if not headers are present", () => {
	const jwt = extractJwt({query: {}, body: {}, headers: {}} as unknown as ExpressRequest);
	expect(jwt).toEqual(null);
});
