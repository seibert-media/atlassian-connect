import fetch from "node-fetch";

/**
 * fetches RSA public key from a given remote host.
 * uses fetch api directly as a jwks-rsa client would expect a JSON Web Key Set Atlassian does not provide
 *
 * @param url
 */
export async function fetchRsaPublicKey({url}: { url: string }): Promise<string> {
	const rsaPublicKeyResponse = await fetch(url);
	if (!rsaPublicKeyResponse.ok) {
		throw new Error(`Could not fetch RSA public key. Host responded with ${rsaPublicKeyResponse.status}. ${rsaPublicKeyResponse.statusText}.`);
	}
	return await rsaPublicKeyResponse.text();
}
