import {createCanonicalRequest, createQueryStringHash, Request} from "atlassian-jwt";

export interface QueryHashValidation {
	successful: boolean,
	error?: string
}

/**
 * Verifies if a queryStringHash matches a given Request
 * mind the Request type: it is provided by "atlassian-jwt". An express request object must be converted first.
 *
 * @see fromExpressRequest
 *
 * @param baseUrl
 * @param qsh
 * @param request
 */
export function verifyQueryHash({
	baseUrl,
	qsh,
	request
}: { baseUrl: string, qsh: string, request: Request }): QueryHashValidation {
	// this verification algorithm was taken from atlassian reference middleware
	// https://bitbucket.org/atlassian/atlassian-connect-express/src/master/lib/middleware/authentication.js
	const expectedHash = createQueryStringHash(request, false, baseUrl);
	if (qsh === expectedHash) {
		return {successful: true};
	}

	// If that didn't verify, it might be a post/put - check the request body too
	const expectedHashRespectingBody = createQueryStringHash(request, true, baseUrl);
	if (qsh === expectedHashRespectingBody) {
		return {successful: true};
	}

	const canonicalRequest = createCanonicalRequest(request, true, baseUrl);
	const error = `Auth failure: Query hash mismatch: Received: "${qsh}" but calculated "${expectedHash} ${expectedHash !== expectedHashRespectingBody && `and ${expectedHashRespectingBody}`}. Requests canonically expression was: "${canonicalRequest}"`;
	return {successful: false, error};

}
