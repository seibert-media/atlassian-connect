import {Request as ExpressRequest} from "express-serve-static-core";

/**
 * Extracts JWT from an Express request, migrated from atlassian-connect-express library where JWT extraction priority is: query > body > header
 * returns null if no JWT is found or incompatible request was found (JWT header not allowed when no body is present, jwt in query AND body not allowed)
 *
 * @param request
 */
export function extractJwt(request: ExpressRequest): string | null {
	const jwtInQuery = request.query?.jwt;
	const jwtInBody = request.body?.jwt;

	if (!jwtInQuery && !request.body) {
		console.warn("Cannot find JWT token in query parameters. " +
			"Please include body-parser middleware and parse the urlencoded body " +
			"(See https://github.com/expressjs/body-parser) if the add-on is rendering in POST mode. " +
			"Otherwise please ensure the jwt parameter is presented in query.");
		return null;
	}

	const jwtInHeader = extractJwtFromAuthorizationHeader(request);

	if (jwtInQuery && jwtInBody) {
		console.warn("JWT token can only appear in either query parameter or request body.");
		return null;
	}

	const jwt = jwtInQuery || jwtInBody || jwtInHeader;

	if (!jwt) {
		console.warn("JWT token not found in request query, body or authorization header.");
		return null;
	}

	return jwt;
}

function extractJwtFromAuthorizationHeader(request: ExpressRequest): string | null {
	const authHeader = request.headers?.authorization;
	if (authHeader && authHeader.indexOf("JWT ") === 0) {
		return authHeader.substring(4);
	}
	return null;
}
