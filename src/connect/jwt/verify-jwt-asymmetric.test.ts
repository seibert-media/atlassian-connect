import {AsymmetricAlgorithm, encodeAsymmetric} from "atlassian-jwt";
import {verifyJwtAsymmetric} from "./verify-jwt-asymmetric";
import {
	PRIVATE_KEY_2_FOR_TESTING_PURPOSES,
	PRIVATE_KEY_FOR_TESTING_PURPOSES, PUBLIC_KEY_2_FOR_TESTING_PURPOSES,
	PUBLIC_KEY_FOR_TESTING_PURPOSES
} from "../../keys-for-tests";


const JWT_BODY = {
	aud: ["valid-audience"],
	iss: "valid-issuer",
	qsh: "c88caad15a1c1a900b8ac08aa9686f4e8184539bea1deda36e2f649430df3239", // for GET request on / without query params
	exp: Math.round(new Date().getTime() / 1000 + 5000)
};

const JWT_HEADER = {
	kid: "123"
};

const DECODE_OPTIONS = {
	expectedAudience: JWT_BODY.aud[0],
	expectedIssuer: JWT_BODY.iss,
	request: {pathname: "/", method: "GET", body: null, query: {}}
};

function getDecodingOfGivenJwtBody(jwtBody: Partial<ReturnType<typeof verifyJwtAsymmetric>>, privateKey = PRIVATE_KEY_FOR_TESTING_PURPOSES, publicKey = PUBLIC_KEY_FOR_TESTING_PURPOSES) {
	const encodedJwt = encodeAsymmetric(jwtBody, privateKey, AsymmetricAlgorithm.RS256, JWT_HEADER);
	return () => verifyJwtAsymmetric(encodedJwt, publicKey, DECODE_OPTIONS);
}

test("Throws on body signed with unknown private key.", () => {
	const jwtBodyMissingAudClaim = {...JWT_BODY, aud: undefined};
	expect(getDecodingOfGivenJwtBody(jwtBodyMissingAudClaim, PRIVATE_KEY_2_FOR_TESTING_PURPOSES)).toThrowError("Signature verification failed for input: ");
});

test("Throws on mismatching public key.", () => {
	const jwtBodyMissingAudClaim = {...JWT_BODY, aud: undefined};
	expect(getDecodingOfGivenJwtBody(jwtBodyMissingAudClaim, PRIVATE_KEY_FOR_TESTING_PURPOSES, PUBLIC_KEY_2_FOR_TESTING_PURPOSES)).toThrowError("Signature verification failed for input: ");
});

test("Throws on missing aud claim.", () => {
	const jwtBodyMissingAudClaim = {...JWT_BODY, aud: undefined};
	expect(getDecodingOfGivenJwtBody(jwtBodyMissingAudClaim)).toThrowError("Jwt token invalid. It is missing a valid aud claim.");
});

test("Throws on multiple wrong aud claims.", () => {
	const jwtBodyWrongAudClaims = {...JWT_BODY, aud: ["some-faked-audience", "another-faked-audience"]};
	expect(getDecodingOfGivenJwtBody(jwtBodyWrongAudClaims)).toThrowError("Jwt token invalid. Expected audience not part of jwt tokens audiences.");
});

test("Throws on missing iss claim.", () => {
	const jwtBodyMissingIssClaim = {...JWT_BODY, iss: undefined};
	expect(getDecodingOfGivenJwtBody(jwtBodyMissingIssClaim)).toThrowError("Jwt token invalid. It is missing an iss claim.");
});

test("Throws on wrong iss claim.", () => {
	const jwtBodyWrongIssClaim = {...JWT_BODY, iss: "some-faked-issuer"};
	expect(getDecodingOfGivenJwtBody(jwtBodyWrongIssClaim)).toThrowError("Jwt token invalid. Invalid issuer.");
});

test("Throws on missing qsh claim.", () => {
	const jwtBodyMissingAudClaim = {...JWT_BODY, qsh: undefined};
	expect(getDecodingOfGivenJwtBody(jwtBodyMissingAudClaim)).toThrowError("Jwt token invalid. Does not contain qsh claim.");
});

test("Throws on wrong qsh claim.", () => {
	const jwtBodyWrongQshClaim = {...JWT_BODY, qsh: "some-faked-qsh"};
	expect(getDecodingOfGivenJwtBody(jwtBodyWrongQshClaim)).toThrowError("Jwt token invalid. Query hash mismatch.");
});

test("Throws on missing exp claim.", () => {
	const jwtBodyMissingExpClaim = {...JWT_BODY, exp: undefined};
	expect(getDecodingOfGivenJwtBody(jwtBodyMissingExpClaim)).toThrowError("Jwt token invalid. Does not contain exp claim.");
});

test("Throws on broken exp claim.", () => {
	const jwtBodyWithBrokenExpClaim = {...JWT_BODY, exp: "broken-exp-claim" as unknown as number};
	expect(getDecodingOfGivenJwtBody(jwtBodyWithBrokenExpClaim)).toThrowError("Jwt token invalid. Does not contain exp claim.");
});

test("Throws on expired exp claim.", () => {
	const jwtBodyWithExpiredExpClaim = {...JWT_BODY, exp: (new Date()).getTime() / 1000 - 5000};
	expect(getDecodingOfGivenJwtBody(jwtBodyWithExpiredExpClaim)).toThrowError("Jwt token invalid. Expired token.");
});

test("Decodes valid jwt.", () => {
	const validJwtBody = {...JWT_BODY};
	expect(getDecodingOfGivenJwtBody(validJwtBody)()).toEqual(JWT_BODY);
});
