import {verifyQueryHash} from "./verify-query-hash";

test("Verifies matching query string hash (get)", () => {
	const {successful, error} = verifyQueryHash({
		baseUrl: "https://example.com",
		qsh: "c88caad15a1c1a900b8ac08aa9686f4e8184539bea1deda36e2f649430df3239",
		request: {
			method: "GET",
			pathname: "/",
			query: {},
			body: {}
		}
	});
	expect(successful).toEqual(true);
	expect(error).toBeUndefined();
});

test("Verifies matching query string hash (post, without query with body)", () => {
	const {successful, error} = verifyQueryHash({
		baseUrl: "https://example.com",
		qsh: "3eca35796a2fb93ddbc0d86ce63a4b28b0fb4dd659618ddfae97075f75857f8c",
		request: {
			method: "POST",
			pathname: "/foo",
			body: {bar: 456}
		}
	});
	expect(error).toBeUndefined();
	expect(successful).toEqual(true);
});

test("Rejects on wrong qsh (post, without query, with body)", () => {
	const {successful, error} = verifyQueryHash({
		baseUrl: "https://example.com",
		qsh: "wrong",
		request: {
			method: "POST",
			pathname: "/foo",
			body: {bar: 456}
		}
	});
	expect(error).toContain("Auth failure");
	expect(successful).toEqual(false);
});

test("Rejects on wrong qsh", () => {
	const {successful, error} = verifyQueryHash({
		baseUrl: "https://example.com",
		qsh: "xxx",
		request: {
			method: "POST",
			pathname: "/foo",
			query: {bar: 123},
			body: {bar: 456}
		}
	});
	expect(error).toContain("Auth failure");
	expect(successful).toEqual(false);
});
