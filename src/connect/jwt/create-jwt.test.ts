import {createJwtForRequestAuthorization} from "./create-jwt";
import {decodeSymmetric, SymmetricAlgorithm} from "atlassian-jwt";
import {JwtPayload} from "jsonwebtoken";

test("Creates JWT", () => {
	const jwt = createJwtForRequestAuthorization({
		baseUrl: "https://app.url",
		expirationInSeconds: 500,
		issuer: "iss",
		request: {method: "PATH", pathname: "/foo"},
		tenantData: {sharedSecret: "xxx", clientKey: "yyy"}
	});
	const jwtPayload: JwtPayload = decodeSymmetric(jwt, "xxx", SymmetricAlgorithm.HS256);
	expect(jwtPayload.iss).toEqual("iss");
	expect(jwtPayload.qsh).toEqual("cf0e8e08769ed6d1499c37bf71769f3f1226cecd9971b735190463dc6c4fcefc");
});

test("Creates JWT using default fallback params", () => {
	const jwt = createJwtForRequestAuthorization({
		baseUrl: "https://app.url",
		issuer: "iss",
		request: {},
		tenantData: {sharedSecret: "xxx", clientKey: "yyy"}
	});
	const jwtPayload: JwtPayload = decodeSymmetric(jwt, "xxx", SymmetricAlgorithm.HS256);
	expect(jwtPayload.iss).toEqual("iss");
	expect(jwtPayload.qsh).toEqual("c88caad15a1c1a900b8ac08aa9686f4e8184539bea1deda36e2f649430df3239");
});

test("Creates JWT using additional claims", () => {
	const jwt = createJwtForRequestAuthorization({
		baseUrl: "https://app.url",
		issuer: "iss",
		request: {},
		tenantData: {sharedSecret: "xxx", clientKey: "yyy"},
		additionalClaims: {sub: "foo", xxx: "bar"}
	});
	const jwtPayload: JwtPayload = decodeSymmetric(jwt, "xxx", SymmetricAlgorithm.HS256);
	expect(jwtPayload.iss).toEqual("iss");
	expect(jwtPayload.sub).toEqual("foo");
	expect(jwtPayload.xxx).toEqual("bar");
});
