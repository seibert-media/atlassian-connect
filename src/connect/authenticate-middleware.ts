import {
	AtlassianConnectConfig,
	AtlassianConnectAuthenticationMiddlewareConfig,
	TenantDataWithSharedSecret
} from "./middleware-common";
import {NextFunction, Response as ExpressResponse, Request as ExpressRequest} from "express-serve-static-core";
import {extractJwt} from "./jwt/extract-jwt";
import {JwtPayload} from "jsonwebtoken";
import {decodeSymmetric, fromExpressRequest, SymmetricAlgorithm} from "atlassian-jwt";
import {QueryHashValidation, verifyQueryHash} from "./jwt/verify-query-hash";
import {sendError} from "./sanitize";

export type AuthenticatedAtlassianRequest =
	ExpressRequest
	& { atlassianVerified: { userAccountId: string, clientKey: string, tenant: TenantDataWithSharedSecret, jwtPayload: JwtPayload } };

/**
 * Returns a middleware that can be used for authentication of user requests.
 * The composed middleware verifies the requests as outlined in the Atlassian Connect documentation using a JWT signed with the sharedSecret of a tenant.
 *
 * @see https://developer.atlassian.com/cloud/confluence/understanding-jwt/

 * @param baseUrl
 * BaseUrl from your atlassian-connect.json.
 * @param fetchTenantByClientKey
 * Method that asynchronously returns the data that you have stored for the tenant given its clientKey. Returned data has to contain at least a "sharedSecret".
 * Return null if no matching tenant can be found by your implementation.
 * @param requestsForQshValidation (optional)
 * The default behaviour of the returned middleware is to use the actual Express request that is passed into it to validate the given JWTs query hash (qsh claim).
 * You can optionally choose to validate the given qsh against another fictitious requests. If one request matches the qsh, the request is assumed to be authenticated.
 * This functionality can be used in combination with createJwtForRequestAuthorization which allows you to create JWTs for such fictitious requests.
 * A client can receive such JWTs and use it to make requests to endpoints protected by the returned middleware.
 *
 * @see create-jwt.ts for code examples
 */
export function composeAtlassianRequestAuthenticationMiddleware({
	baseUrl,
	fetchTenantByClientKey,
	requestsForQshValidation
}: AtlassianConnectConfig & AtlassianConnectAuthenticationMiddlewareConfig) {
	return function atlassianRequestAuthenticationMiddleware(req: AuthenticatedAtlassianRequest, res: ExpressResponse, next: NextFunction): void {
		const jwt = extractJwt(req);
		if (!jwt) {
			sendError({res, message: "Could not find JWT."});
			return;
		}

		let unverifiedClaims: JwtPayload;

		try {
			unverifiedClaims = decodeSymmetric(jwt, "", SymmetricAlgorithm.HS256, true); // decode without verification;
		} catch (e) {
			sendError({res, message: "Invalid JWT." + e});
			return;
		}

		const issuer = unverifiedClaims.iss;
		if (!issuer) {
			sendError({res, message: "JWT did not contain the issuer (iss) claim"});
			return;
		}

		fetchTenantByClientKey(issuer).then(tenant => {
			if (!tenant?.sharedSecret) {
				sendError({res, message: "Could not obtain shared secret stored for client"});
				return;
			}

			let verifiedClaims: JwtPayload;
			try {
				verifiedClaims = decodeSymmetric(jwt, tenant.sharedSecret, SymmetricAlgorithm.HS256, false); // decode with verification;
			} catch (e) {
				sendError({res, message: "Unable to decode JWT." + e});
				return;
			}

			if (!verifiedClaims.exp || typeof verifiedClaims.exp !== "number" || Math.round((new Date()).getTime() / 1000) >= verifiedClaims.exp) {
				sendError({res, message: "Jwt token invalid. Does not contain an valid exp claim."});
				return;
			}

			const qshValidationResult: { successful: boolean, errors: string[] } = (requestsForQshValidation || [fromExpressRequest(req)])
				.map(request =>
					verifyQueryHash({
						baseUrl,
						qsh: verifiedClaims.qsh,
						request: request
					}))
				.reduce((v1: { successful: false, errors: [] }, v2: QueryHashValidation) => ({
					successful: v2.successful || v1.successful,
					errors: [...v1.errors, v2.error].filter(e => e),
				}), {successful: false, errors: []});

			if (!qshValidationResult.successful) {
				sendError({res, message: "Jwt token invalid. Does not contain an valid qsh claim. " + qshValidationResult.errors.join(", ")});
				return;
			}

			req.atlassianVerified = {
				clientKey: verifiedClaims.iss,
				userAccountId: verifiedClaims.sub,
				jwtPayload: verifiedClaims,
				tenant
			};

			next();
		}).catch(e => {
			sendError({res, status: 400, message: "Request could not be validated." + e});
		});
	};
}