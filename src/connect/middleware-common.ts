import {Request as AtlassianJwtRequest} from "atlassian-jwt";

export interface AtlassianConnectConfig {
	baseUrl: string;
}

export type TenantDataWithSharedSecret = { sharedSecret: string } & unknown;
export type TenantDataWithClientKey = { clientKey: string } & unknown;
export type TenantByClientKeySupplier = (clientKey: string) => Promise<TenantDataWithSharedSecret | null>;

export interface AtlassianConnectAuthenticationMiddlewareConfig {
	fetchTenantByClientKey: TenantByClientKeySupplier
	requestsForQshValidation?: AtlassianJwtRequest[]
}
