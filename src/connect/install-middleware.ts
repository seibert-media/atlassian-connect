import {NextFunction, Request as ExpressRequest, Response as ExpressResponse} from "express-serve-static-core";
import {verifyJwtAsymmetric} from "./jwt/verify-jwt-asymmetric";
import {decodeObjectFromBase64} from "./jwt/decode-base64";
import {fetchRsaPublicKey} from "./jwt/fetch-rsa-key";
import {fromExpressRequest} from "atlassian-jwt";
import {AtlassianConnectConfig} from "./middleware-common";
import {JwtPayload} from "jsonwebtoken";
import {sendError} from "./sanitize";

const RSA_PUBLIC_KEY_HOST = "https://connect-install-keys.atlassian.com/";
export type AuthenticatedInstallationRequest = ExpressRequest & { validatedJwtPayload: JwtPayload };

/**
 * Returns a middleware that can be used for authentication of install and uninstall lifecycle hook requests in Atlassian Connect ecosystem.
 * The composed middleware verifies the requests as outlined in the Atlassian Connect documentation using a asymmetric encryption and a RSA public key
 * provided from an Atlassian Host.
 *
 * @see https://community.developer.atlassian.com/t/action-required-atlassian-connect-installation-lifecycle-security-improvements/49046
 * @see https://developer.atlassian.com/cloud/confluence/understanding-jwt/
 *
 * You have to provide the baseUrl of your app. This is most probably the host you provide as a "baseUrl" in your atlassian-connect.json.
 *
 * Example usage:
 *
 * app.post('/lifecycle/installed/', [composeAtlassianConnectInstallationMiddleware({baseUrl: ATLASSIAN_CONNECT_FILE.baseUrl})], async (req: Request & ReqWithFirebaseUser, res: Response) => {
 *     console.log('Atlassian Connect Lifecycle Hook triggered: /installed', req.body);
 *     await persistInstallation(db, req.body);
 *     res.send();
 * });
 *
 * @param baseUrl
 * baseUrl from your atlassian-connect.json.
 */
export function composeAtlassianConnectInstallationMiddleware({baseUrl}: AtlassianConnectConfig) {
	return function authenticateAtlassianInstallationRequest(req: AuthenticatedInstallationRequest, res: ExpressResponse, next: NextFunction): void {
		const authorizationHeader = req.get("Authorization");
		if (!authorizationHeader) {
			sendError({res, message: "Could not find Authorization header."});
			return;
		}
		const encodedJwt = authorizationHeader.substring(4); // format is "JWT eyFoo123..."
		const [encodedJwtHeader] = encodedJwt.split("."); // contains encodedJwtHeader, encodedJwtBody and encodedJwtSignature
		const jwtHeader = decodeObjectFromBase64<{ kid?: string }>(encodedJwtHeader);

		if (!jwtHeader?.kid) {
			sendError({res, status: 400, message: "Jwt header has an unexpected form."});
			return;
		}

		if (!req.body?.clientKey) {
			sendError({res, message: "Could not find clientKey on body."});
			return;
		}

		fetchRsaPublicKey({url: RSA_PUBLIC_KEY_HOST + jwtHeader.kid}).then(rsaPublicKey => {
			try {
				req.validatedJwtPayload = verifyJwtAsymmetric(encodedJwt, rsaPublicKey, {
					expectedAudience: baseUrl,
					expectedIssuer: req.body.clientKey,
					request: fromExpressRequest(req),
				});
				next();
			} catch (e) {
				sendError({res, message: "Could not decode jwt token." + e});
			}
		}).catch(error => {
			sendError({res, message: "Could not obtain rsaPublicKey" + error});
			return;
		});
	};
}
