# Atlassian Connect Tooling

Provides authentication & utility methods for Atlassian Connect apps running on Express or solutions with a compatible
API like Firebase Cloud Functions.

## Motivation

Atlassian Connect is a development framework for extending Atlassian cloud products. Operating a Connect app requires
the constant adaption of new security standards and authentication mechanisms provided by Atlassian.

There are two official libraries that automatically comply with new requirements (Atlassian Connect Spring Boot and
Atlassian Connect Express).

Atlassian Connect Express comes along with a whole development setup and is a bit biased on what databases to use. As we
are using our own backend (GCP) and our Atlassian Connect functionality is just a single piece within a larger
application we could not use this framework as a whole.

Unfortunately Atlassian Connect Express does not export reusable functionality if you have just Express in use. Most
methods within their package require an (unstable to mock) ```addon``` object that is based on the configuration of the
SDK.

**This package provides reusable helper methods if you run Atlassian Connect on Express or solutions with a compatible
API like Firebase Cloud Functions.**

## Features

### Composable Install and Uninstall lifecycle hook middleware function

Atlassian announced changes to their Install- and Uninstall-lifecycle hook requirements. From 29th October 2021 these
requests are decrypted asymmetrically.
See [announcement](https://community.developer.atlassian.com/t/action-required-atlassian-connect-installation-lifecycle-security-improvements/49046)
and [documentation](https://developer.atlassian.com/cloud/confluence/understanding-jwt/#verifying-a-asymmetric-jwt-token-for-install-callbacks)
.

This packages exports ```composeAtlassianConnectInstallationMiddleware``` to get a middleware function that handles all
this authentication stuff.

#### Example usage with Express

```ts
import {composeAtlassianConnectInstallationMiddleware} from "@seibert/atlassian-connect-tooling";

// if you use this before 29th Oct 2021 remember to opt-in to the new handshake in your atlassian-connect.json by adding "apiMigrations": {"signed-install": true}.
const installAuthentication = composeAtlassianConnectInstallationMiddleware({baseUrl: "https://example.com"});

app.post('/lifecycle/installed/', [installAuthentication], (req: Request, res: Response) => {
	// request is authenticated, process installation here. Necessary information are on request body.
	handleInstall(req.body).then(res.send);
});

app.post('/lifecycle/uninstalled/', [installAuthentication], (req: Request, res: Response) => {
	// request is authenticated, process uninstall here. Necessary information are on request body.
	handleUninstall(req.body).then(res.send);
});
```

### Composable request authentication middleware

This packages exports ```composeAtlassianRequestAuthenticationMiddleware``` to get a middleware function that handles
authentication of JWTs. The composed middleware verifies the requests as outlined in the Atlassian Connect documentation
using a JWT signed with the sharedSecret of a tenant. You have to provide an async. method (fetchTenantByClientKey) that
returns the client data you have persisted for the tenant. The object has to at least contain a sharedSecret.

```ts
const atlassianMiddlewareConfig = {
	baseUrl: ATLASSIAN_CONNECT_FILE.baseUrl,
	fetchTenantByClientKey(clientKey: string) {
		return getInstallation(db, clientKey);
	}
};
const atlassianAuthentication = composeAtlassianRequestAuthenticationMiddleware(atlassianMiddlewareConfig);
app.get("/macros/some-macro", [atlassianAuthentication], (req: Request & AuthenticatedAtlassianRequest, res: Response) => {
	// request is authenticated, verified information on req.atlassianVerified
	res.send();
});
```

### Create JWTs for custom authentication

This packages exports ```createJwtForRequestAuthorization``` which allows you to create own JWTs that may be accepted by
the middlewares functions from ```composeAtlassianRequestAuthenticationMiddleware```. See docs
of ```createJwtForRequestAuthorization``` for more information and code examples.

#### Example usage with Next.js

Express middlewares can be used with Next.js using a wrapper function. See
their [Connect/Express middleware support documentation
](https://nextjs.org/docs/api-routes/api-middlewares#connectexpress-middleware-support).
